from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import hashlib
from modeles import modeles
from cryptography.fernet import Fernet
from schemas.schemas import User
from modeles import modeles
from sqlalchemy.orm import Session


SQLALCHEMY_DATABASE_URL = "sqlite:///database/myDatabase.db"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

f = Fernet(b'7Uzv2ECpEFXcYUU-7nJWA5n9LptOTa1w9lMNdlkQOUM=')

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

def encrypt(text: str):
    return f.encrypt(text.encode())

#Permet de hasher un mot de passe
def sha256_hash(password: str):
    return hashlib.sha256(f'{password}'.encode('utf-8')).hexdigest()

def ajouter_utilisateur(username, password, prenom, nom, email, role, id_entreprise):
    for session in get_db():
        new_user = modeles.User(identifiant=encrypt(username), password=sha256_hash(password), prenom=encrypt(prenom), nom=encrypt(nom), email=encrypt(email), role=role, id_entreprise=id_entreprise)
        session.add(new_user)
        session.commit()
        
        
def ajouter_planning(id_planning, id_entreprise, nom):
    for session in get_db():
        new_planning = modeles.Planning(id_planning=id_planning, id_entreprise=id_entreprise, nom=nom)
        session.add(new_planning)
        session.commit()
        
def ajouter_entreprise(id_entreprise, nom):
    for session in get_db():
        new_company = modeles.Entreprises(id_entreprise=id_entreprise, nom=nom)
        session.add(new_company)
        session.commit()
        
def ajouter_activite(id_activite, nom, date, heure_debut, heure_fin, id_utilisateur, id_planning):
    for session in get_db():
        new_event = modeles.Activites(id_activite=id_activite, nom=nom, date=date, heure_debut=heure_debut, heure_fin=heure_fin, id_utilisateur=id_utilisateur, id_planning=id_planning)
        session.add(new_event)
        session.commit()
        

        
        
# def init_data():
 
#     ajouter_utilisateur('johndoe', 'password1', 'john', 'doe', 'john@email.com', 'maintainer', 0)
#     ajouter_utilisateur('yan', 'password1', 'yan', 'souetre', 'yan@email.com', 'admin', 2),
#     ajouter_utilisateur('janedoe', 'password2', 'Jane', 'Doe', 'jane@email.com', 'admin', 1),
#     ajouter_utilisateur('bobsmith', 'password3', 'Bob', 'Smith', 'bob@email.com', 'user', 1),
#     ajouter_utilisateur('alice', 'password4', 'Alice', 'Johnson', 'alice@email.com', 'user', 2)

#     ajouter_planning(1, 1, 'Planning 1')
#     ajouter_planning(2, 1, 'Planning 2')
#     ajouter_planning(3, 2, 'Planning 3')
#     ajouter_planning(4, 2, 'Planning 4')

#     ajouter_entreprise(1, 'EFFICOM')
#     ajouter_entreprise(2, 'ESGI')


                
            