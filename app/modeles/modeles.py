from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.types import Boolean, Date, DateTime, Float, Integer, Text, Time, Interval

Base = declarative_base()

class User(Base): 
    __tablename__ = "utilisateurs";
     
    id_utilisateur = Column(Integer, primary_key=True, index=True);
    nom = Column(String, index=True);
    prenom = Column(String, index=True);
    email = Column(String, unique=True, index=True);
    identifiant = Column(String, index=True);
    password = Column(String);
    id_entreprise = Column(Integer, ForeignKey("entreprises.id_entreprise"));
    role = Column(String, index=True);



class Entreprises(Base):
    __tablename__ = "entreprises"

    id_entreprise = Column(Integer, primary_key=True, index=True)
    nom = Column(String, index=True)



class Planning(Base): 
    __tablename__ = "planning"

    id_planning = Column(Integer, primary_key=True, index=True)
    id_entreprise = Column(Integer, ForeignKey("entreprises.id_entreprise"))
    nom = Column(String, index=True, nullable=True)
    
    

class Activites(Base): 
    __tablename__ = "activites"

    id_activite = Column(Integer, primary_key=True, index=True)
    nom = Column(String, index=True)
    date = Column(Date, index=True)
    heure_debut = Column(Time, unique=True, index=True)
    heure_fin = Column(Time, unique=True, index=True)
    id_utilisateur = Column(Integer, ForeignKey("utilisateurs.id_utilisateur"))
    id_planning = Column(Integer, ForeignKey("planning.id_planning"))
 
    
    
class Notifications(Base): 
    __tablename__ = "notifications"

    id_notification = Column(Integer, primary_key=True, index=True)
    nom = Column(String, index=True)
    description = Column(String, index=True)
    statut = Column(String, index=True)
    id_utilisateur = Column(Integer, ForeignKey("utilisateurs.id_utilisateur"))