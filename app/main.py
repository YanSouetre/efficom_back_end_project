#System imports
from enum import Enum


#Libs imports
from fastapi import FastAPI, status, Depends
from pydantic import BaseModel


#Local imports
from routers import users, auth, entreprises, planning
from database import database
from modeles import modeles


modeles.Base.metadata.create_all(bind=database.engine)
# database.init_data()


app = FastAPI()



app.include_router(users.router, responses = {204: {"description": "No Content"}})
app.include_router(planning.router, responses = {204: {"description": "No Content"}})
app.include_router(entreprises.router, responses = {204: {"description": "No Content"}})
app.include_router(auth.router, responses = {204: {"description": "No Content"}})