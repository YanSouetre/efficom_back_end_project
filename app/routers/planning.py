#-------------------------------[Imports]-------------------------------
#---[System imports]---
from enum import Enum
import hashlib

#---[Libs imports]---
from fastapi import FastAPI, status, Depends, APIRouter, HTTPException
from sqlalchemy.orm import Session


#---[Local imports]---
from database.database import SessionLocal, engine
from schemas import schemas
from modeles import modeles

modeles.Base.metadata.create_all(bind=engine)


#-------------------------------[Routeur Utilisateur]-------------------------------
router = APIRouter()

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

#Créer un planning
@router.post("/planning/", response_model=schemas.Planning, tags=["Planning"])
def create_planning(planning: schemas.PlanningCreate, db: Session = Depends(get_db)):
    db_planning = db.query(modeles.Planning).filter(modeles.Planning.id_planning == planning.id_planning).first()
    if db_planning:
        raise HTTPException(status_code=400, detail=f"Planning {db_planning.id_planning} already exists")
    db_planning = modeles.Planning(id_planning=planning.id_planning,
                                   id_entreprise=planning.id_entreprise)
    db.add(db_planning)
    db.commit()
    db.refresh(db_planning)
    return db_planning

#Récupere un planning par son email
@router.get("/planning/{planning_id}", response_model=schemas.Planning, tags=["Planning"])
def get_planning(planning_id: int, db: Session = Depends(get_db)):
    db_planning = db.query(modeles.Planning).filter(modeles.Planning.id_planning == planning_id).first()
    if db_planning is None:
        raise HTTPException(status_code=404, detail="Planning not found")
    return db_planning

#modifie un planning par son id
@router.put("/planning/{planning_id}", response_model=schemas.Planning, tags=["Planning"])
def update_planning(planning_id: int, planning: schemas.PlanningUpdate, db: Session = Depends(get_db)):
    db_planning = db.query(modeles.Planning).filter(modeles.Planning.id_planning == planning_id).first()
    if db_planning is None:
        raise HTTPException(status_code=404, detail="Planning not found")
    for field, value in planning.dict(exclude_unset=True).items():
        setattr(db_planning, field, value)
    db.commit()
    db.refresh(db_planning)
    return db_planning

#Supprime un planning
@router.delete("/planning/{planning_id}", status_code=204, tags=["Planning"])
def delete_planning(planning_id: int, db: Session = Depends(get_db)):
    db_planning = db.query(modeles.Planning).filter(modeles.Planning.id_planning == planning_id).first()
    if db_planning is None:
        raise HTTPException(status_code=404, detail="Planning not found")
    db.delete(db_planning)
    db.commit()
    return None

#Récupere tous les planning
@router.get("/planning/", tags=["Planning"])
def get_all_planning(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    plannings = db.query(modeles.Planning).offset(skip).limit(limit).all()
    return plannings


