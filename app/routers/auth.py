#-------------------------------[Imports]-------------------------------
#---[System imports]---
import hashlib
from typing import Annotated

#---[Libs imports]---
from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from pydantic import BaseModel
from sqlalchemy.orm import Session
from cryptography.fernet import Fernet

#---[Local imports]---
from schemas import schemas
from modeles.modeles import User
from database.database import SessionLocal, engine

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")

#-------------------------------[Fonctions d'authentification]-------------------------------

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

f = Fernet(b'7Uzv2ECpEFXcYUU-7nJWA5n9LptOTa1w9lMNdlkQOUM=')

def encrypt(text: str):
    return f.encrypt(text.encode())

def decrypt(text: str):
    return f.decrypt(text).decode()

def sha256_hash(password: str):
    return hashlib.sha256(f'{password}'.encode('utf-8')).hexdigest()

async def decode_token(token: Annotated[str, Depends(oauth2_scheme)], db: Session = Depends(get_db)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        decoded = jwt.decode(token, JWT_KEY, algorithms=["HS256"])
        user = db.query(User).filter_by(
            id_utilisateur=decoded["id_utilisateur"]).first().__dict__
        user["prenom"] = decrypt(user["prenom"])
        user["nom"] = decrypt(user["nom"])
        user["email"] = decrypt(user["email"])
        if user == None:
            raise credentials_exception
    except JWTError:
        return credentials_exception
    return user



#-------------------------------[Routeur d'authentification]-------------------------------
router = APIRouter()

JWT_KEY = "bfizbizndcizufizubonzeuizufui"

@router.post("/login", tags=["Login"])
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    db = SessionLocal()
    users = db.query(User).all()
    
    for user in users:
        if decrypt(user.identifiant) == form_data.username:
            if user.password == sha256_hash(form_data.password):
                myUser = user
                myUser = User(
                    id_utilisateur=myUser.id_utilisateur,
                ).__dict__
                data = dict()
                data["id_utilisateur"] = myUser["id_utilisateur"]
                jwt_token = jwt.encode(data, JWT_KEY, algorithm="HS256")
                return {"access_token": jwt_token, "token_type": "bearer"}
    raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail="Identifiant ou mot de passe incorrect")

    
@router.get("/items/", tags=["Login"])
async def read_items(user: Annotated[User, Depends(decode_token)]):
    return "Worked"
