#-------------------------------[Imports]-------------------------------
#---[System imports]---
from enum import Enum
import hashlib

#---[Libs imports]---
from fastapi import FastAPI, status, Depends, APIRouter, HTTPException
from sqlalchemy.orm import Session


#---[Local imports]---
from database.database import SessionLocal, engine
from schemas import schemas
from modeles import modeles

modeles.Base.metadata.create_all(bind=engine)

#-------------------------------[Routeur Entreprises]-------------------------------
router = APIRouter()

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

# Récupérer toutes les entreprises
@router.get("/entreprises/", tags=["Entreprises"])
async def get_all_entreprises(db: Session = Depends(get_db)):
    entreprises = db.query(modeles.Entreprises).all()
    return entreprises

# Créer une entreprise
@router.post("/entreprises/", response_model=schemas.Entreprise, tags=["Entreprises"])
async def create_entreprise(entreprise: schemas.EntrepriseCreate, db: Session = Depends(get_db)):
    db_entreprise = db.query(modeles.Entreprises).filter(modeles.Entreprises.nom == entreprise.nom).first()
    if db_entreprise:
        raise HTTPException(status_code=400, detail=f"Entreprise {db_entreprise.nom} already exists")
    db_entreprise = modeles.Entreprises(nom=entreprise.nom)
    db.add(db_entreprise)
    db.commit()
    db.refresh(db_entreprise)
    return db_entreprise

# Obtenir une entreprise par son ID
@router.get("/entreprises/{entreprise_id}", response_model=schemas.Entreprise, tags=["Entreprises"])
async def get_entreprise(entreprise_id: int, db: Session = Depends(get_db)):
    entreprise = db.query(modeles.Entreprises).filter(modeles.Entreprises.id_entreprise == entreprise_id).first()
    if not entreprise:
        raise HTTPException(status_code=404, detail="Entreprise not found")
    return entreprise

# Supprimer une entreprise par son ID
@router.delete("/entreprises/{entreprise_id}", status_code=status.HTTP_204_NO_CONTENT, tags=["Entreprises"])
async def delete_entreprise(entreprise_id: int, db: Session = Depends(get_db)):
    entreprise = db.query(modeles.Entreprises).filter(modeles.Entreprises.id_entreprise == entreprise_id).first()
    if not entreprise:
        raise HTTPException(status_code=404, detail="Entreprise not found")
    db.delete(entreprise)
    db.commit()
    return None

# Mettre à jour une entreprise par son ID
@router.put("/entreprises/{entreprise_id}", response_model=schemas.Entreprise, tags=["Entreprises"])
async def update_entreprise(entreprise_id: int, entreprise: schemas.EntrepriseUpdate, db: Session = Depends(get_db)):
    db_entreprise = db.query(modeles.Entreprises).filter(modeles.Entreprises.id_entreprise == entreprise_id).first()
    if not db_entreprise:
        raise HTTPException(status_code=404, detail="Entreprise not found")
    db_entreprise.nom = entreprise.nom
    db.commit()
    db.refresh(db_entreprise)
    return db_entreprise

#-------------------------------[Fonctions entreprises]-------------------------------

# Obtenir une entreprise par son nom
def get_entreprise_by_name(db: Session, nom: str):
    return db.query(modeles.Entreprises).filter(modeles.Entreprises.nom == nom).first()
