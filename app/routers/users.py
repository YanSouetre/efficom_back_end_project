#-------------------------------[Imports]-------------------------------
#---[System imports]---
from enum import Enum
import hashlib

#---[Libs imports]---
from fastapi import FastAPI, status, Depends, APIRouter, HTTPException
from sqlalchemy.orm import Session
from cryptography.fernet import Fernet

#---[Local imports]---
from database.database import SessionLocal, engine
from schemas import schemas
from modeles import modeles
from routers.auth import decode_token

modeles.Base.metadata.create_all(bind=engine)


#-------------------------------[Routeur Utilisateur]-------------------------------
router = APIRouter()

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
        
f = Fernet(b'7Uzv2ECpEFXcYUU-7nJWA5n9LptOTa1w9lMNdlkQOUM=')

def encrypt(text: str):
    return f.encrypt(text.encode())
            
            
#Créer un utilisateur        
@router.post("/user/", response_model=schemas.User, tags=["Utilisateurs"])
async def create_user(user: schemas.UserCreate, db: SessionLocal = Depends(get_db), current_user: schemas.User = Depends(decode_token)):
    db_user = await get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail=f"l'email {db_user.email} existe déjà")
    user = await create_user(db=db, user=user)
    return user
    
#affiche tous les utilisateurs
@router.get("/users/", tags=["Utilisateurs"])
async def get_all_users(db: SessionLocal = Depends(get_db)):
    users = get_users(db=db)
    return users
    
#-------------------------------[Fonctions utilisateurs]-------------------------------

#Permet de hasher un mot de passe
async def sha256_hash(password: str):
    return hashlib.sha256(f'{password}'.encode('utf-8')).hexdigest()

async def get_user(db: Session, user_id: int):
    return db.query(modeles.User).filter(modeles.User.id == user_id).first()

async def get_user_by_email(db: Session, email: str):
    return db.query(modeles.User).filter(modeles.User.email == email).first()

#Récupère une liste d'utilisateurs
def get_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(modeles.User).offset(skip).limit(limit).all()

#Créer un utilisateur
async def create_user(db: Session, user: schemas.UserCreate):
    hashed_password = await sha256_hash(user.password)
    db_user = modeles.User(email=encrypt(user.email), 
                           password=hashed_password,
                           identifiant=(user.identifiant),
                           prenom=encrypt(user.prenom),
                           nom=encrypt(user.nom),
                           role=user.role,
                           id_entreprise=user.id_entreprise)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user 