from pydantic import BaseModel
from datetime import date, time


class UserBase(BaseModel):
    email: str
    identifiant: str
    prenom: str
    nom: str
    role: str
    id_entreprise: int


class UserCreate(UserBase):
    password: str

class User(UserBase):
    id_utilisateur: int

    class Config:
        orm_mode = True
        
        

class EntrepriseBase(BaseModel):
    nom: str

class EntrepriseCreate(EntrepriseBase):
    pass

class Entreprise(EntrepriseBase):
    id_entreprise: int

    class Config:
        orm_mode = True
            
class EntrepriseUpdate(BaseModel):
    nom: str


class PlanningBase(BaseModel):
    nom: str = None


class PlanningCreate(PlanningBase):
    id_planning: int
    id_entreprise: int

class PlanningUpdate(BaseModel):
    id_entreprise: int
    nom: str = None
    id_entreprise: int

class Planning(PlanningBase):
    id_planning: int

    class Config:
        orm_mode = True


class ActivitesBase(BaseModel):
    nom: str
    date: date
    heure_debut: time
    heure_fin: time
    id_utilisateur: int


class ActivitesCreate(ActivitesBase):
    pass


class ActivitesUpdate(BaseModel):
    nom: str
    date: date
    heure_debut: time
    heure_fin: time
    id_utilisateur: int

class Activites(ActivitesBase):
    id_activite: int

    class Config:
        orm_mode = True