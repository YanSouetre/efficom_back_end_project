## Setup

If you choose to install python, don't forget to install the requirements.

```bash
pip install -r requirements.txt
```

### Run with Docker

```bash
docker-compose up --build


Please refer to the following table to understand the meaning of the characters in the database tables.

#### Users table

| id  | identifiant     | password_hash      | prenom!         | nom           |  email          | role       | company_id |
| --- | -----------     | ------------------ | --------------  | ----------    | ----------      | ---------- |
| 1   | 'johndoe',       'password1',         'john',           'doe',          'john@email.com', 'maintainer', 0
| 2   | 'yan',           'password1',         'yan',            'souetre',      'yan@email.com',  'admin',      2
| 3   | 'janedoe',       'password2',         'Jane',           'Doe',           'jane@email.com', 'admin',     1
| 4   | 'bobsmith',      'password3',         'Bob',            'Smith',         'bob@email.com',  'user',      1  
| 5   | 'alice',         'password4',         'Alice',          'Johnson',       'alice@email.com', 'user', 



## Evaluation grid

### 1. Structure du projet et organisation du code (35 points)

- [x] README.md clair et bien documenté (sachant qu'un README ne me permettant pas d'exécuter le code entrainera une réduction de la note globale de 33%) (5 points)
- [x] Organisation des fichiers et dossiers (5 points)
- [x] Utilisation appropriée des modules et packages (5 points)
- [x] Lisibilité et propreté du code (10 points)
- [x] Commentaires lisibles et faisant sens (5 points)
- [x] Bonne utilisation du git (commits de bonne taille, messages faisant sens) (5 points)

### 2. Implémentation des standards appris en cours (35 points)

- [x] Utilisation de pydantic (5 points)
- [x] Section d'import bien formaté (system, libs, local), et chemins absolus et non relatifs Requirements.py avec versions fixes (5 points)
- [x] Définition du type de donnée en retour des fonctions. (5 points)
- [x] Bonne utilisation des path & query parameters (10 points)
- [x] Retour des bons codes HTTP (10 points)

### 3. Implémentation des fonctionnalités demandées (85 points)

- [x] Connexion à la base de données (30 points)
- [x] Gestion des utilisateurs (15 points)
- [x] Gestion des plannings (15 points)
- [x] Gestion des activités (15 points)
- [x] Gestion des entreprises (10 points)

### 4. Sécurité (20 points)

- [x] Utilisation de tokens pour l'authentification (JWT) (5 points)
- [x] Validation et vérification des données entrantes avec modèles pydantics, not (5 points)
- [x] Gestion des erreurs et exceptions (5 points)
- [x] Sécurisation de la connexion à la base de données (5 points)